import java.util.*;

public class RECUR_60160150 {
	static int row;
	static int col;
	static char arr[][];
	static ArrayList<String> ALS = new ArrayList<String>();
	
	
	
	public static boolean findPath(int x, int y, int count,String text) {

		if (x < 0 || x >= row || y < 0 || y >= col)
			return false;



		count++ ;
		text = text+arr[x][y] ;
		
		
		if (count == 6) {
//			System.out.println(text);
			if(ALS.contains(text)) {
				
			}else {
				ALS.add(text);
			}
			
			return false;
		}
		if (findPath(x, y + 1,count,text)) {
//			System.out.println(text);
			return false;
		}
		if (findPath(x + 1, y ,count,text)) {
//			System.out.println(text);
			return false;
		}
		if (findPath(x, y - 1,count,text)) {
//			System.out.println(text);
			return false;
		}
		if (findPath(x - 1, y ,count,text)) {
//			System.out.println(text);
			return false;
		}


		return false;  
	}

	public static void print(char[][] arr) {
		System.out.println("-----------------");
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		row =5;
		col =5;

		arr = new char[row][col];

		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				arr[i][j] = kb.next().charAt(0);

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
//				System.out.println("-----------");
				findPath(i, j,0,"");
			}
		}
//		for (String string : ALS) {
//			System.out.println(string);
//		}
		System.out.println(ALS.size());
		

	}

}
